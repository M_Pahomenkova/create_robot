package bodyParts;

/**
 * Created by Marina on 10.12.2015.
 */
public interface Part {
    public int getWeight();
    public String getDetail();
    public String getMaterial();
}
