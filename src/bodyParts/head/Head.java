package bodyParts.head;

import bodyParts.BodyPart;
import bodyParts.Part;
import material.Material;

/**
 * Created by Marina on 10.12.2015.
 */
public class Head extends BodyPart implements Part{
    public Head(String detail, Material material){
        this.detail=detail;
        this.material=material;
        this.weight= setWeight(material.getMinWeight(), material.getMaxWeight());
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String getDetail() {
        return detail;
    }

    @Override
    public String getMaterial() {
        return material.getMaterialName();
    }
}
