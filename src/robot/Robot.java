package robot;

import bodyParts.Part;
import bodyParts.arms.Arms;
import bodyParts.body.Body;
import bodyParts.feet.Feet;
import bodyParts.head.Head;
import material.CastIron;
import material.Material;
import material.Plastic;
import material.Steel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marina on 11.12.2015.
 */
public class Robot {
    private Part arms;
    private Part feet;
    private Part head;
    private Part body;
    private int weightAll;
    public Robot(){
        arms=new Arms("руки",setMaterial());
        feet=new Feet("ноги",setMaterial());
        head=new Head("голова",setMaterial());
        body=new Body("тело",setMaterial());
        setWeightAll();
    }

    private Material setMaterial(){
        int rand = (int)(Math.random()*3);
        Material material=null;
        switch (rand) {
            case 0:
                material = new Steel();
                break;
            case 1:
                material = new Plastic();
                break;
            case 2:
                material = new CastIron();
                break;
        }
       return material;
    }

    private void setWeightAll(){
        weightAll = arms.getWeight()+head.getWeight()+body.getWeight()+feet.getWeight();
    }

    public int getWeightAll(){
        return weightAll;
    }

    public void printWeightAll(){
        System.out.println("Общий вес робота равен: "+ weightAll);
    }

    public void printParts(){
        List<Part> parts = new ArrayList<Part>();
        parts.add(arms);
        parts.add(head);
        parts.add(body);
        parts.add(feet);
        for (Part part : parts) {
            System.out.println(part.getDetail()+": "+part.getMaterial()+"; вес: "+part.getWeight());

        }
    }

    public int division(int weightAll){
        if(weightAll==0) {
            System.out.println("Вес=0");
            return 0;
        }
        System.out.println("Вес/2="+weightAll);
        weightAll=division(weightAll/2);
        return weightAll;
    }
}
