package material;

/**
 * Created by Marina on 11.12.2015.
 */
public class Plastic implements Material{

    @Override
    public String getMaterialName() {
        return "пластик";
    }

    @Override
    public int getMinWeight() {
        return 1;
    }

    @Override
    public int getMaxWeight() {
        return 9;
    }
}
