package material;

/**
 * Created by Marina on 11.12.2015.
 */
public class Steel implements Material{

    @Override
    public String getMaterialName() {
        return "сталь";
    }

    @Override
    public int getMinWeight() {
        return 9;
    }

    @Override
    public int getMaxWeight() {
        return 20;
    }
}
