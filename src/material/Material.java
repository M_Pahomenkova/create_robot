package material;

/**
 * Created by Marina on 11.12.2015.
 */
public interface Material {
     String getMaterialName();
     int getMinWeight();
     int getMaxWeight();
}
