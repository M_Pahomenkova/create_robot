package material;

/**
 * Created by Marina on 11.12.2015.
 */
public class CastIron implements Material{

    @Override
    public String getMaterialName() {
        return "чугун";
    }

    @Override
    public int getMinWeight() {
        return 21;
    }

    @Override
    public int getMaxWeight() {
        return 30;
    }
}
